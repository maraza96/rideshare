package com.msdevelopers.ryde;

import java.util.ArrayList;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.msdevelopers.ryde.dao.RideOwnerDAO;
import com.msdevelopers.ryde.model.RideOwner;
import com.msdevelopers.ryde.model.RideOwner;


@RestController
@RequestMapping(value = "/api")
public class RideOwnerController {
	RideOwnerDAO DAObj;
	
	@RequestMapping(value = "/ride-owners", method = RequestMethod.GET)
	public ResponseEntity getAll(){
		DAObj=new RideOwnerDAO();
		try {
			
			return ResponseEntity.ok().body(DAObj.getAll());
		}catch(Exception ex) {	
			System.out.println(ex.getMessage());
			return new ResponseEntity(HttpStatus.NOT_FOUND);
		
		}
			
	}
	
	@RequestMapping(value = "/ride-owners/{id}", method = RequestMethod.GET)
	public ResponseEntity get(@PathVariable("id") int id){
		DAObj=new RideOwnerDAO();
		try {
			
			return ResponseEntity.ok().body(DAObj.getByPK(id));
		}catch(Exception ex) {	
			System.out.println(ex.getMessage());
			return new ResponseEntity(HttpStatus.NOT_FOUND);
		
		}
		
		
	}
	
	@RequestMapping(value = "/ride-owners", method = RequestMethod.POST)
	public ResponseEntity post(@RequestBody RideOwner rideOwner) {
		DAObj=new RideOwnerDAO();
		try {
			DAObj.save(rideOwner);
			return new ResponseEntity(HttpStatus.OK);
		}catch(Exception ex) {	
			System.out.println(ex.getMessage());
			return new ResponseEntity(HttpStatus.NOT_FOUND);
		
		}
	}
	@RequestMapping(value = "/ride-owners/{id}", method = RequestMethod.PUT)
	public ResponseEntity update(@PathVariable("id") int id, @RequestBody RideOwner rideOwner) {
		DAObj=new RideOwnerDAO();
		try {
			DAObj.update(rideOwner);
			return new ResponseEntity(HttpStatus.OK);
		}catch(Exception ex) {	
			System.out.println(ex.getMessage());
			return new ResponseEntity(HttpStatus.NOT_FOUND);
		
		}	
	}
	@DeleteMapping("/ride-owners/{id}")
	public ResponseEntity delete(@PathVariable("id") int id) {
		DAObj=new RideOwnerDAO();
		try {
			DAObj.delete(id);
			return new ResponseEntity(HttpStatus.OK);
		}catch(Exception ex) {	
			System.out.println(ex.getMessage());
			return new ResponseEntity(HttpStatus.NOT_FOUND);
		
		}
	}

}
