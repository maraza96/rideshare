package com.msdevelopers.ryde;

import java.util.ArrayList;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.msdevelopers.ryde.dao.AdminDAO;
import com.msdevelopers.ryde.model.Admin;

@RestController
@RequestMapping(value = "/api")
public class AdminController {
	AdminDAO DAObj;
	
	@RequestMapping(value = "/admins", method = RequestMethod.GET)
	public ResponseEntity getAll(){
		DAObj=new AdminDAO();
		try {
			
			return ResponseEntity.ok().body(DAObj.getAll());
		}catch(Exception ex) {	
			System.out.println(ex.getMessage());
			return new ResponseEntity(HttpStatus.NOT_FOUND);
		
		}
			
	}
	
	@RequestMapping(value = "/admins/{id}", method = RequestMethod.GET)
	public ResponseEntity get(@PathVariable("id") int id){
		DAObj=new AdminDAO();
		try {
			
			return ResponseEntity.ok().body(DAObj.getByPK(id));
		}catch(Exception ex) {	
			System.out.println(ex.getMessage());
			return new ResponseEntity(HttpStatus.NOT_FOUND);
		
		}
		
		
	}
	
	@RequestMapping(value = "/admins", method = RequestMethod.POST)
	public ResponseEntity post(@RequestBody Admin admin) {
		DAObj=new AdminDAO();
		try {
			DAObj.save(admin);
			return new ResponseEntity(HttpStatus.OK);
		}catch(Exception ex) {	
			System.out.println(ex.getMessage());
			return new ResponseEntity(HttpStatus.NOT_FOUND);
		
		}
	}
	@RequestMapping(value = "/admins/{id}", method = RequestMethod.PUT)
	public ResponseEntity update(@PathVariable("id") int id, @RequestBody Admin admin) {
		DAObj=new AdminDAO();
		try {
			DAObj.update(admin);
			return new ResponseEntity(HttpStatus.OK);
		}catch(Exception ex) {	
			System.out.println(ex.getMessage());
			return new ResponseEntity(HttpStatus.NOT_FOUND);
		
		}	
	}
	@DeleteMapping("/admins/{id}")
	public ResponseEntity delete(@PathVariable("id") int id) {
		DAObj=new AdminDAO();
		try {
			DAObj.delete(id);
			return new ResponseEntity(HttpStatus.OK);
		}catch(Exception ex) {	
			System.out.println(ex.getMessage());
			return new ResponseEntity(HttpStatus.NOT_FOUND);
		
		}
	}

	
}
