package com.msdevelopers.ryde.model;

import org.joda.time.DateTime;

public class CancelledRide {
	private int id;
	
	private String cancelledBy; //constant to see who cancelled ride
	private Ride ride; //for which ride
	private DateTime cancelTime;
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public Ride getRide() {
		return ride;
	}
	public void setRide(Ride ride) {
		this.ride = ride;
	}
	public DateTime getCancelTime() {
		return cancelTime;
	}
	public void setCancelTime(DateTime cancelTime) {
		this.cancelTime = cancelTime;
	}
	public String getCancelledBy() {
		return cancelledBy;
	}
	public void setCancelledBy(String cancelledBy) {
		this.cancelledBy = cancelledBy;
	}
	
	
}
