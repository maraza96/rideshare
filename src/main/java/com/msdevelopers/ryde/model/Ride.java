package com.msdevelopers.ryde.model;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;


@Entity
@Table(name = "rides")
public class Ride {
	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;
	
	private String type;
	private String status;
	
	@OneToOne(cascade=CascadeType.ALL)
	@JoinColumn(name = "rideOwnerId")
	private RideOwner rideOwner;
	
	@OneToOne(cascade=CascadeType.ALL)
	@JoinColumn(name = "scheduleId")
	private RideSchedule rideSchedule;
	
	@OneToMany(fetch = FetchType.LAZY,mappedBy = "ride")
	private List<AssociatedRider> associatedRiders;
	
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		
		this.type = type;
	}
	public RideOwner getRideOwner() {
		rideOwner.setVehicle(null);
		return rideOwner;
	}
	public void setRideOwner(RideOwner rideOwner) {
		this.rideOwner = rideOwner;
	}
	public RideSchedule getRideSchedule() {
		return rideSchedule;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public void setRideSchedule(RideSchedule rideSchedule) {
		this.rideSchedule = rideSchedule;
	}
	public List<AssociatedRider> getAssociatedRiders() {
		return associatedRiders;
	}
	public void setAssociatedRiders(List<AssociatedRider> associatedRiders) {
		this.associatedRiders = associatedRiders;
	}
		
}
