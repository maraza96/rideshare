package com.msdevelopers.ryde.model;

import java.sql.Timestamp;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name = "ride_requests")
public class RideRequest {
	
	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;
	
	private double pickupLongitude;
	private double pickupLatitude;
	private double dropoffLongitude;
	private double dropoffLatitude;
	private Timestamp pickupTime;
	
	@OneToOne(cascade=CascadeType.ALL)
	@JoinColumn(name = "rideId")
	private Ride ride;
	
	@OneToOne(cascade=CascadeType.ALL)
	@JoinColumn(name = "riderId")
	private Rider rider;
	
	@OneToOne(cascade=CascadeType.ALL)
	@JoinColumn(name = "riderScheduleId")
	private RiderSchedule riderSchedule;
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public Ride getRide() {
		return ride;
	}
	public void setRide(Ride ride) {
		this.ride = ride;
	}
	public Rider getRider() {
		return rider;
	}
	public void setRider(Rider rider) {
		this.rider = rider;
	}
	public void setRiderSchedule(RiderSchedule riderSchedule){
		this.riderSchedule=riderSchedule;
	}
	public RiderSchedule getRiderSchedule() {
		return riderSchedule;
	}
	public double getPickupLongitude() {
		return pickupLongitude;
	}
	public void setPickupLongitude(double pickupLongitude) {
		this.pickupLongitude = pickupLongitude;
	}
	public double getPickupLatitude() {
		return pickupLatitude;
	}
	public void setPickupLatitude(double pickupLatitude) {
		this.pickupLatitude = pickupLatitude;
	}
	public double getDropoffLongitude() {
		return dropoffLongitude;
	}
	public void setDropoffLongitude(double dropoffLongitude) {
		this.dropoffLongitude = dropoffLongitude;
	}
	public double getDropoffLatitude() {
		return dropoffLatitude;
	}
	public void setDropoffLatitude(double dropoffLatitude) {
		this.dropoffLatitude = dropoffLatitude;
	}
	public Timestamp getPickupTime() {
		return pickupTime;
	}
	public void setPickupTime(Timestamp pickupTime) {
		this.pickupTime = pickupTime;
	}
}
