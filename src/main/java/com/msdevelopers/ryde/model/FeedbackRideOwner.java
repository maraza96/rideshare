package com.msdevelopers.ryde.model;

public class FeedbackRideOwner extends Feedback{
	private RideOwner rideOwner;
	private Rider givenBy;

	public RideOwner getRideOwner() {
		return rideOwner;
	}

	public void setRideOwner(RideOwner rideOwner) {
		this.rideOwner = rideOwner;
	}

	public Rider getGivenBy() {
		return givenBy;
	}

	public void setGivenBy(Rider givenBy) {
		this.givenBy = givenBy;
	}

}
