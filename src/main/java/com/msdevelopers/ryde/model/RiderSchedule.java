package com.msdevelopers.ryde.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;


@Entity
@Table(name = "rider_schedules")
public class RiderSchedule {
	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;
	
	private boolean SAT;
	private boolean SUN;
	private boolean MON;
	private boolean TUE;
	private boolean WED;
	private boolean THU;
	private boolean FRI;
	
	
	public boolean isSAT() {
		return SAT;
	}
	public void setSAT(boolean sAT) {
		SAT = sAT;
	}
	public boolean isSUN() {
		return SUN;
	}
	public void setSUN(boolean sUN) {
		SUN = sUN;
	}
	public boolean isMON() {
		return MON;
	}
	public void setMON(boolean mON) {
		MON = mON;
	}
	public boolean isTUE() {
		return TUE;
	}
	public void setTUE(boolean tUE) {
		TUE = tUE;
	}
	public boolean isWED() {
		return WED;
	}
	public void setWED(boolean wED) {
		WED = wED;
	}
	public boolean isTHU() {
		return THU;
	}
	public void setTHU(boolean tHU) {
		THU = tHU;
	}
	public boolean isFRI() {
		return FRI;
	}
	public void setFRI(boolean fRI) {
		FRI = fRI;
	}
}
