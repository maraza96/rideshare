package com.msdevelopers.ryde.model;

import org.joda.time.DateTime;

public class CompletedRide {
	private int id;
	private int fare;
	private DateTime dropTime;
	private Ride ride; //for which ride
	private Rider rider; //who was given rider
	private FeedbackRider feedbackRider; //Feedbacks for rider
	private FeedbackRideOwner feedbackRideOwner;
	
	//private Location dropOffLocation; 
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getFare() {
		return fare;
	}
	public void setFare(int fare) {
		this.fare = fare;
	}
	public DateTime getDropTime() {
		return dropTime;
	}
	public void setDropTime(DateTime dropTime) {
		this.dropTime = dropTime;
	}
	public FeedbackRider getFeedbackRider() {
		return feedbackRider;
	}
	public void setFeedbackRider(FeedbackRider feedbackRider) {
		this.feedbackRider = feedbackRider;
	}
	public FeedbackRideOwner getFeedbackRideOwner() {
		return feedbackRideOwner;
	}
	public void setFeedbackRideOwner(FeedbackRideOwner feedbackRideOwner) {
		this.feedbackRideOwner = feedbackRideOwner;
	}
	public Rider getRider() {
		return rider;
	}
	public void setRider(Rider rider) {
		this.rider = rider;
	}
	public Ride getRide() {
		return ride;
	}
	public void setRide(Ride ride) {
		this.ride = ride;
	}

}
