package com.msdevelopers.ryde.model;

import java.io.Serializable;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

//import org.hibernate.annotations.Type;
//import org.joda.time.DateTime;
import java.sql.Timestamp;


@Entity
@Table(name = "ride_schedules")
public class RideSchedule{
	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;
	
	private Timestamp rideTime;
	private double startLongitude;
	private double startLatitude;
	private double endLongitude;
	private double endLatitude;
	
	@OneToOne(cascade=CascadeType.ALL)
	@JoinColumn(name = "scheduleDaysId")
	private ScheduleDays scheduleDays;
	
	
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public Timestamp getRideTime() {
		return rideTime;
	}
	public void setRideTime(Timestamp rideTime) {
		this.rideTime = rideTime;
	}
	public double getStartLongitude() {
		return startLongitude;
	}
	public void setStartLongitude(double startLongitude) {
		this.startLongitude = startLongitude;
	}
	public double getStartLatitude() {
		return startLatitude;
	}
	public void setStartLatitude(double startLatitude) {
		this.startLatitude = startLatitude;
	}
	public double getEndLongitude() {
		return endLongitude;
	}
	public void setEndLongitude(double endLongitude) {
		this.endLongitude = endLongitude;
	}
	public double getEndLatitude() {
		return endLatitude;
	}
	public void setEndLatitude(double endLatitude) {
		this.endLatitude = endLatitude;
	}
	public ScheduleDays getScheduleDays() {
		return scheduleDays;
	}
	public void setScheduleDays(ScheduleDays scheduleDays) {
		this.scheduleDays = scheduleDays;
	}
	
}
