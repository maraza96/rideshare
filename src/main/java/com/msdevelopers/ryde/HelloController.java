package com.msdevelopers.ryde;

import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


import org.hibernate.Session;
import org.hibernate.Transaction;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.msdevelopers.ryde.dao.RideOwnerDAO;
import com.msdevelopers.ryde.dao.RiderDAO;
import com.msdevelopers.ryde.model.Admin;
import com.msdevelopers.ryde.model.RideOwner;
import com.msdevelopers.ryde.model.Rider;
import com.msdevelopers.ryde.model.Vehicle;


@Controller
public class HelloController{
	@RequestMapping(value="/welcome", method = RequestMethod.GET)
	public ModelAndView welcome(HttpServletRequest req, 
			HttpServletResponse res) throws Exception{ // any method name
		ModelAndView mav= new ModelAndView("home");
		RideOwnerDAO sdao=new RideOwnerDAO();
		try {
			ArrayList<RideOwner> r=sdao.getAll();
			System.out.println(r.get(1).getFirstName());
		}
		catch(Exception ex) {
			System.out.println(ex.getMessage());
		}
		
		return mav;
	}
	@RequestMapping("/home")
	public ModelAndView home(HttpServletRequest req, 
			HttpServletResponse res) throws Exception{ // any method name
		ModelAndView mav= new ModelAndView("home");
		mav.addObject("message","Hi! Welcome to first Spring MVC Home");
		return mav;
	}
}
