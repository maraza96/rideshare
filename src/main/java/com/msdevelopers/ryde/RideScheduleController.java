package com.msdevelopers.ryde;

import java.util.ArrayList;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.msdevelopers.ryde.dao.RideScheduleDAO;
import com.msdevelopers.ryde.model.RideSchedule;

@RestController
@RequestMapping(value = "/api")
public class RideScheduleController {
	RideScheduleDAO DAObj;
	
	@RequestMapping(value = "/ride-schedules", method = RequestMethod.GET)
	public ResponseEntity getAll(){
		DAObj=new RideScheduleDAO();
		try {
			
			return ResponseEntity.ok().body(DAObj.getAll());
		}catch(Exception ex) {	
			System.out.println(ex.getMessage());
			return new ResponseEntity(HttpStatus.NOT_FOUND);
		
		}
			
	}
	
	@RequestMapping(value = "/ride-schedules/{id}", method = RequestMethod.GET)
	public ResponseEntity get(@PathVariable("id") int id){
		DAObj=new RideScheduleDAO();
		try {
			
			return ResponseEntity.ok().body(DAObj.getByPK(id));
		}catch(Exception ex) {	
			System.out.println(ex.getMessage());
			return new ResponseEntity(HttpStatus.NOT_FOUND);
		
		}
		
		
	}
	
	@RequestMapping(value = "/ride-schedules", method = RequestMethod.POST)
	public ResponseEntity post(@RequestBody RideSchedule rideSchedule) {
		DAObj=new RideScheduleDAO();
		try {
			DAObj.save(rideSchedule);
			return new ResponseEntity(HttpStatus.OK);
		}catch(Exception ex) {	
			System.out.println(ex.getMessage());
			return new ResponseEntity(HttpStatus.NOT_FOUND);
		
		}
	}
	@RequestMapping(value = "/ride-schedules/{id}", method = RequestMethod.PUT)
	public ResponseEntity update(@PathVariable("id") int id, @RequestBody RideSchedule rideSchedule) {
		DAObj=new RideScheduleDAO();
		try {
			DAObj.update(rideSchedule);
			return new ResponseEntity(HttpStatus.OK);
		}catch(Exception ex) {	
			System.out.println(ex.getMessage());
			return new ResponseEntity(HttpStatus.NOT_FOUND);
		
		}	
	}
	@DeleteMapping("/ride-schedules/{id}")
	public ResponseEntity delete(@PathVariable("id") int id) {
		DAObj=new RideScheduleDAO();
		try {
			DAObj.delete(id);
			return new ResponseEntity(HttpStatus.OK);
		}catch(Exception ex) {	
			System.out.println(ex.getMessage());
			return new ResponseEntity(HttpStatus.NOT_FOUND);
		
		}
	}

}
