package com.msdevelopers.ryde;

import org.hibernate.HibernateException;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

import com.fasterxml.classmate.AnnotationConfiguration;

public class HibernateUtilities {
	private static SessionFactory sess;
	
	static {
		try{
			
			Configuration cfg=new Configuration();
			cfg.configure();
			sess=cfg.buildSessionFactory();
			
			
		}catch(HibernateException e) {
			System.out.println(e.getMessage().toString());
		}
		
	}
	public static SessionFactory getSessionFactory() {
		return sess;
	}
	
}
