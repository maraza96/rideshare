package com.msdevelopers.ryde;

import java.util.ArrayList;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.msdevelopers.ryde.dao.RiderDAO;
import com.msdevelopers.ryde.model.Rider;

@RestController
@RequestMapping(value = "/api")
public class RiderController {
	RiderDAO DAObj;
	
	@RequestMapping(value = "/riders", method = RequestMethod.GET)
	public ResponseEntity getAll(){
		DAObj=new RiderDAO();
		try {
			
			return ResponseEntity.ok().body(DAObj.getAll());
		}catch(Exception ex) {	
			System.out.println(ex.getMessage());
			return new ResponseEntity(HttpStatus.NOT_FOUND);
		
		}
		
		
	}
	
	@RequestMapping(value = "/riders/{id}", method = RequestMethod.GET)
	public ResponseEntity get(@PathVariable("id") int id){
		DAObj=new RiderDAO();
		try {
			
			return ResponseEntity.ok().body(DAObj.getByPK(id));
		}catch(Exception ex) {	
			System.out.println(ex.getMessage());
			return new ResponseEntity(HttpStatus.NOT_FOUND);
		
		}
		
		
	}
	
	@RequestMapping(value = "/riders", method = RequestMethod.POST)
	public ResponseEntity post(@RequestBody Rider rider) {
		DAObj=new RiderDAO();
		try {
			DAObj.save(rider);
			return new ResponseEntity(HttpStatus.OK);
		}catch(Exception ex) {	
			System.out.println(ex.getMessage());
			return new ResponseEntity(HttpStatus.NOT_FOUND);
		
		}
	}
	@RequestMapping(value = "/riders/{id}", method = RequestMethod.PUT)
	public ResponseEntity update(@PathVariable("id") int id, @RequestBody Rider rider) {
		DAObj=new RiderDAO();
		try {
			DAObj.update(rider);
			return new ResponseEntity(HttpStatus.OK);
		}catch(Exception ex) {	
			System.out.println(ex.getMessage());
			return new ResponseEntity(HttpStatus.NOT_FOUND);
		
		}	
	}
	@DeleteMapping("/riders/{id}")
	public ResponseEntity delete(@PathVariable("id") int id) {
		DAObj=new RiderDAO();
		try {
			DAObj.delete(id);
			return new ResponseEntity(HttpStatus.OK);
		}catch(Exception ex) {	
			System.out.println(ex.getMessage());
			return new ResponseEntity(HttpStatus.NOT_FOUND);
		
		}
	}
}
