package com.msdevelopers.ryde.dao;

import java.util.ArrayList;

import com.msdevelopers.ryde.model.FeedbackRideOwner;

public interface FeedbackRideOwnerDAI {
	public void save(FeedbackRideOwner feedbackRideOwner) throws Exception;
	public void update(FeedbackRideOwner feedbackRideOwner) throws Exception;
	public void delete(FeedbackRideOwner feedbackRideOwner) throws Exception;
	public FeedbackRideOwner getByPK(int id) throws Exception;
	public ArrayList<FeedbackRideOwner> getAll() throws Exception;
}
