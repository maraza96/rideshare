package com.msdevelopers.ryde.dao;

import java.util.ArrayList;

import com.msdevelopers.ryde.model.FeedbackRider;

public interface FeedbackRiderDAI {
	public void save(FeedbackRider feedbackRider) throws Exception;
	public void update(FeedbackRider feedbackRider) throws Exception;
	public void delete(FeedbackRider feedbackRider) throws Exception;
	public FeedbackRider getByPK(int id) throws Exception;
	public ArrayList<FeedbackRider> getAll() throws Exception;
}
