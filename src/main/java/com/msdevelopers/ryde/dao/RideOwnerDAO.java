package com.msdevelopers.ryde.dao;

import java.util.ArrayList;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.query.Query;

import com.msdevelopers.ryde.HibernateUtilities;
import com.msdevelopers.ryde.model.RideOwner;

public class RideOwnerDAO implements RideOwnerDAI {
	private Session session=HibernateUtilities.getSessionFactory().openSession();
	@Override
	public int save(RideOwner rideOwner) throws Exception {
		
		Transaction t=session.beginTransaction();
		session.save(rideOwner);
		t.commit();
		return rideOwner.getId();

	}

	@Override
	public void update(RideOwner rideOwner) throws Exception {
		Transaction t=session.beginTransaction();
		//RideOwner tempRideOwner=(RideOwner)session.get(RideOwner.class,RideOwner.getId());
		/*if(tempRideOwner==null) {
			throw new Exception("ID not found");
		}*/
		
		session.update(rideOwner);
		t.commit();

	}

	@Override
	public void delete(int id) throws Exception {
		Transaction t=session.beginTransaction();
		RideOwner tempRideOwner=(RideOwner)session.load(RideOwner.class,id);
		if(tempRideOwner==null) {
			throw new Exception("ID not found");
		}
		session.remove(tempRideOwner);
		t.commit();
		session.flush();

	}

	@Override
	public RideOwner getByPK(int id) throws Exception {
		Transaction t=session.beginTransaction();
		RideOwner tempRideOwner=(RideOwner)session.get(RideOwner.class,id);
		if(tempRideOwner==null) {
			throw new Exception("ID not found");
		}

		session.flush();
		return tempRideOwner;
	}

	@Override
	public ArrayList<RideOwner> getAll() throws Exception {
		Transaction t=session.beginTransaction();
		  CriteriaBuilder cb = session.getCriteriaBuilder();
	      CriteriaQuery<RideOwner> cq = cb.createQuery(RideOwner.class);
	      Root<RideOwner> root = cq.from(RideOwner.class);
	      cq.select(root);
	      
	      Query<RideOwner> query = session.createQuery(cq);
	      session.flush();
	      return (ArrayList<RideOwner>)query.getResultList();
	}

}
