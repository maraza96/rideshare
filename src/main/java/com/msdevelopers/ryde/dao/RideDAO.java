package com.msdevelopers.ryde.dao;

import java.util.ArrayList;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.query.Query;
import org.springframework.web.bind.annotation.RequestBody;

import com.msdevelopers.ryde.HibernateUtilities;
import com.msdevelopers.ryde.model.Ride;
import com.msdevelopers.ryde.model.Ride;

public class RideDAO implements RideDAI {
	private Session session=HibernateUtilities.getSessionFactory().openSession();
	@Override
	public int save(@RequestBody Ride ride) throws Exception{
		
		Transaction t=session.beginTransaction();
		session.save(ride);
		t.commit();
		return ride.getId();
	}

	@Override
	public void update(Ride ride) throws Exception{

		Transaction t=session.beginTransaction();
		//Ride tempRide=(Ride)session.get(Ride.class,ride.getId());
		/*if(tempRide==null) {
			throw new Exception("ID not found");
		}*/
		
		session.update(ride);
		t.commit();
	}

	@Override
	public void delete(int id) throws Exception{
		Transaction t=session.beginTransaction();
		Ride tempRide=(Ride)session.load(Ride.class,id);
		if(tempRide==null) {
			throw new Exception("ID not found");
		}
		session.remove(tempRide);
		t.commit();
		session.flush();
	}

	@Override
	public Ride getByPK(int id) throws Exception{
		Transaction t=session.beginTransaction();
		Ride tempRide=(Ride)session.get(Ride.class,id);
		if(tempRide==null) {
			throw new Exception("ID not found");
		}

		session.flush();
		return tempRide;
	}

	
	@Override
	public ArrayList<Ride> getAll() throws Exception{
		
		  Transaction t=session.beginTransaction();
		  CriteriaBuilder cb = session.getCriteriaBuilder();
	      CriteriaQuery<Ride> cq = cb.createQuery(Ride.class);
	      Root<Ride> root = cq.from(Ride.class);
	      cq.select(root);
	      
	      Query<Ride> query = session.createQuery(cq);
	      session.flush();
	      return (ArrayList<Ride>)query.getResultList();
	}

}
