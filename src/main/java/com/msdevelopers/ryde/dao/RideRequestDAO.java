package com.msdevelopers.ryde.dao;

import java.util.ArrayList;

import org.hibernate.query.Query;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import org.hibernate.Session;
import org.hibernate.Transaction;
import org.springframework.web.bind.annotation.RequestBody;

import com.msdevelopers.ryde.HibernateUtilities;
import com.msdevelopers.ryde.model.RideRequest;

public class RideRequestDAO implements RideRequestDAI {
	private Session session=HibernateUtilities.getSessionFactory().openSession();
	@Override
	public int save(@RequestBody RideRequest RideRequest) throws Exception{
		
		Transaction t=session.beginTransaction();
		session.save(RideRequest);
		t.commit();
		return RideRequest.getId();
	}

	@Override
	public void update(RideRequest RideRequest) throws Exception{

		Transaction t=session.beginTransaction();
		//RideRequest tempRideRequest=(RideRequest)session.get(RideRequest.class,RideRequest.getId());
		/*if(tempRideRequest==null) {
			throw new Exception("ID not found");
		}*/
		
		session.update(RideRequest);
		t.commit();
	}

	@Override
	public void delete(int id) throws Exception{
		Transaction t=session.beginTransaction();
		RideRequest tempRideRequest=(RideRequest)session.load(RideRequest.class,id);
		if(tempRideRequest==null) {
			throw new Exception("ID not found");
		}
		session.remove(tempRideRequest);
		t.commit();
		session.flush();
	}

	@Override
	public RideRequest getByPK(int id) throws Exception{
		Transaction t=session.beginTransaction();
		RideRequest tempRideRequest=(RideRequest)session.get(RideRequest.class,id);
		if(tempRideRequest==null) {
			throw new Exception("ID not found");
		}

		session.flush();
		return tempRideRequest;
	}

	
	@Override
	public ArrayList<RideRequest> getAll() throws Exception{
		
		  Transaction t=session.beginTransaction();
		  CriteriaBuilder cb = session.getCriteriaBuilder();
	      CriteriaQuery<RideRequest> cq = cb.createQuery(RideRequest.class);
	      Root<RideRequest> root = cq.from(RideRequest.class);
	      cq.select(root);
	      
	      Query<RideRequest> query = session.createQuery(cq);
	      session.flush();
	      return (ArrayList<RideRequest>)query.getResultList();
	}

	

}
