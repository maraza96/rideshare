package com.msdevelopers.ryde.dao;

import java.util.ArrayList;

import com.msdevelopers.ryde.model.Admin;

public interface AdminDAI {
	public int save(Admin admin) throws Exception;
	public void update(Admin admin) throws Exception;
	public void delete(int id) throws Exception;
	public Admin getByPK(int id) throws Exception;
	public ArrayList<Admin> getAll() throws Exception;
}
