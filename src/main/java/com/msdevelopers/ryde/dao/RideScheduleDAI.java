package com.msdevelopers.ryde.dao;

import java.util.ArrayList;

import com.msdevelopers.ryde.model.RideSchedule;

public interface RideScheduleDAI {
	public int save(RideSchedule schedule) throws Exception;
	public void update(RideSchedule schedule) throws Exception;
	public void delete(int id) throws Exception;
	public RideSchedule getByPK(int id) throws Exception;
	public ArrayList<RideSchedule> getAll() throws Exception;
}
