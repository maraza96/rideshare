package com.msdevelopers.ryde.dao;

import java.util.ArrayList;

import com.msdevelopers.ryde.model.RideRequest;

public interface RideRequestDAI {
	public int save(RideRequest rideRequest) throws Exception;
	public void update(RideRequest rideRequest) throws Exception;
	public void delete(int id) throws Exception;
	public RideRequest getByPK(int id) throws Exception;
	public ArrayList<RideRequest> getAll() throws Exception;
}
