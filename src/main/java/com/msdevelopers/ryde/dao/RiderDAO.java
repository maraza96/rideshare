package com.msdevelopers.ryde.dao;

import java.util.ArrayList;

import org.hibernate.query.Query;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import org.hibernate.Session;
import org.hibernate.Transaction;
import org.springframework.web.bind.annotation.RequestBody;

import com.msdevelopers.ryde.HibernateUtilities;
import com.msdevelopers.ryde.model.Rider;

public class RiderDAO implements RiderDAI {
	private Session session=HibernateUtilities.getSessionFactory().openSession();
	@Override
	public int save(@RequestBody Rider rider) throws Exception{
		
		Transaction t=session.beginTransaction();
		session.save(rider);
		t.commit();
		return rider.getId();
	}

	@Override
	public void update(Rider rider) throws Exception{

		Transaction t=session.beginTransaction();
		//Rider tempRider=(Rider)session.get(Rider.class,rider.getId());
		/*if(tempRider==null) {
			throw new Exception("ID not found");
		}*/
		
		session.update(rider);
		t.commit();
	}

	@Override
	public void delete(int id) throws Exception{
		Transaction t=session.beginTransaction();
		Rider tempRider=(Rider)session.load(Rider.class,id);
		if(tempRider==null) {
			throw new Exception("ID not found");
		}
		session.remove(tempRider);
		t.commit();
		session.flush();
	}

	@Override
	public Rider getByPK(int id) throws Exception{
		Transaction t=session.beginTransaction();
		Rider tempRider=(Rider)session.get(Rider.class,id);
		if(tempRider==null) {
			throw new Exception("ID not found");
		}

		session.flush();
		return tempRider;
	}

	
	@Override
	public ArrayList<Rider> getAll() throws Exception{
		
		  Transaction t=session.beginTransaction();
		  CriteriaBuilder cb = session.getCriteriaBuilder();
	      CriteriaQuery<Rider> cq = cb.createQuery(Rider.class);
	      Root<Rider> root = cq.from(Rider.class);
	      cq.select(root);
	      
	      Query<Rider> query = session.createQuery(cq);
	      session.flush();
	      return (ArrayList<Rider>)query.getResultList();
	}

	

}
