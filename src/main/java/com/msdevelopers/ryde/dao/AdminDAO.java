package com.msdevelopers.ryde.dao;

import java.util.ArrayList;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.query.Query;
import org.springframework.web.bind.annotation.RequestBody;

import com.msdevelopers.ryde.HibernateUtilities;
import com.msdevelopers.ryde.model.Admin;
import com.msdevelopers.ryde.model.Admin;

public class AdminDAO implements AdminDAI {
	private Session session=HibernateUtilities.getSessionFactory().openSession();
	
	@Override
	public int save(@RequestBody Admin admin) throws Exception{
		
		Transaction t=session.beginTransaction();
		session.save(admin);
		t.commit();
		return admin.getId();
	}

	@Override
	public void update(Admin admin) throws Exception{

		Transaction t=session.beginTransaction();
		//Admin tempAdmin=(Admin)session.get(Admin.class,admin.getId());
		/*if(tempAdmin==null) {
			throw new Exception("ID not found");
		}*/
		
		session.update(admin);
		t.commit();
	}

	@Override
	public void delete(int id) throws Exception{
		Transaction t=session.beginTransaction();
		Admin tempAdmin=(Admin)session.load(Admin.class,id);
		if(tempAdmin==null) {
			throw new Exception("ID not found");
		}
		session.remove(tempAdmin);
		t.commit();
		session.flush();
	}

	@Override
	public Admin getByPK(int id) throws Exception{
		Transaction t=session.beginTransaction();
		Admin tempAdmin=(Admin)session.get(Admin.class,id);
		if(tempAdmin==null) {
			throw new Exception("ID not found");
		}

		session.flush();
		return tempAdmin;
	}

	
	@Override
	public ArrayList<Admin> getAll() throws Exception{
		
		  Transaction t=session.beginTransaction();
		  CriteriaBuilder cb = session.getCriteriaBuilder();
	      CriteriaQuery<Admin> cq = cb.createQuery(Admin.class);
	      Root<Admin> root = cq.from(Admin.class);
	      cq.select(root);
	      
	      Query<Admin> query = session.createQuery(cq);
	      session.flush();
	      return (ArrayList<Admin>)query.getResultList();
	}

}
