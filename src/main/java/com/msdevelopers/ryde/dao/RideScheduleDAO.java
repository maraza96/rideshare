package com.msdevelopers.ryde.dao;

import java.util.ArrayList;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.query.Query;
import org.springframework.web.bind.annotation.RequestBody;

import com.msdevelopers.ryde.HibernateUtilities;
import com.msdevelopers.ryde.model.RideSchedule;
import com.msdevelopers.ryde.model.RideSchedule;

public class RideScheduleDAO implements RideScheduleDAI {
	private Session session=HibernateUtilities.getSessionFactory().openSession();
	
	@Override
	public int save(@RequestBody RideSchedule rideSchedule) throws Exception{
		
		Transaction t=session.beginTransaction();
		session.save(rideSchedule);
		t.commit();
		return rideSchedule.getId();
	}

	@Override
	public void update(RideSchedule rideSchedule) throws Exception{

		Transaction t=session.beginTransaction();
		//RideSchedule tempRideSchedule=(RideSchedule)session.get(RideSchedule.class,rideSchedule.getId());
		/*if(tempRideSchedule==null) {
			throw new Exception("ID not found");
		}*/
		
		session.update(rideSchedule);
		t.commit();
	}

	@Override
	
	public void delete(int id) throws Exception{
		Transaction t=session.beginTransaction();
		RideSchedule tempRideSchedule=(RideSchedule)session.load(RideSchedule.class,id);
		if(tempRideSchedule==null) {
			throw new Exception("ID not found");
		}
		session.remove(tempRideSchedule);
		t.commit();
		session.flush();
	}

	@Override
	public RideSchedule getByPK(int id) throws Exception{
		Transaction t=session.beginTransaction();
		RideSchedule tempRideSchedule=(RideSchedule)session.get(RideSchedule.class,id);
		if(tempRideSchedule==null) {
			throw new Exception("ID not found");
		}

		session.flush();
		return tempRideSchedule;
	}

	
	@Override
	public ArrayList<RideSchedule> getAll() throws Exception{
		
		  Transaction t=session.beginTransaction();
		  CriteriaBuilder cb = session.getCriteriaBuilder();
	      CriteriaQuery<RideSchedule> cq = cb.createQuery(RideSchedule.class);
	      Root<RideSchedule> root = cq.from(RideSchedule.class);
	      cq.select(root);
	      
	      Query<RideSchedule> query = session.createQuery(cq);
	      session.flush();
	      return (ArrayList<RideSchedule>)query.getResultList();
	}

}
