package com.msdevelopers.ryde.dao;

import java.util.ArrayList;

import com.msdevelopers.ryde.model.Ride;

public interface RideDAI {
	public int save(Ride ride) throws Exception;
	public void update(Ride ride) throws Exception;
	public void delete(int id) throws Exception;
	public Ride getByPK(int id) throws Exception;
	public ArrayList<Ride> getAll() throws Exception;
}
