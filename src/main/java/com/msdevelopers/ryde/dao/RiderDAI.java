package com.msdevelopers.ryde.dao;

import java.util.ArrayList;

import com.msdevelopers.ryde.model.Rider;

public interface RiderDAI {
	public int save(Rider rider) throws Exception;
	public void update(Rider rider) throws Exception;
	public Rider getByPK(int id) throws Exception;
	public ArrayList<Rider> getAll() throws Exception;
	public void delete(int id) throws Exception;
}
