package com.msdevelopers.ryde.dao;

import java.util.ArrayList;

import com.msdevelopers.ryde.model.RideOwner;

public interface RideOwnerDAI {
	public int save(RideOwner rideOwner) throws Exception;
	public void update(RideOwner rideOwner) throws Exception;
	public void delete(int id) throws Exception;
	public RideOwner getByPK(int id) throws Exception;
	public ArrayList<RideOwner> getAll() throws Exception;
}
