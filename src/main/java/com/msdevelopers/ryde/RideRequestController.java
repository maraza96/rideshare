package com.msdevelopers.ryde;

import java.util.ArrayList;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.msdevelopers.ryde.dao.RideRequestDAO;
import com.msdevelopers.ryde.model.RideRequest;

@RestController
@RequestMapping(value = "/api")
public class RideRequestController {
	RideRequestDAO DAObj;
	
	@RequestMapping(value = "/ride-requests", method = RequestMethod.GET)
	public ResponseEntity getAll(){
		DAObj=new RideRequestDAO();
		try {
			
			return ResponseEntity.ok().body(DAObj.getAll());
		}catch(Exception ex) {	
			System.out.println(ex.getMessage());
			return new ResponseEntity(HttpStatus.NOT_FOUND);
		
		}
			
	}
	
	@RequestMapping(value = "/ride-requests/{id}", method = RequestMethod.GET)
	public ResponseEntity get(@PathVariable("id") int id){
		DAObj=new RideRequestDAO();
		try {
			
			return ResponseEntity.ok().body(DAObj.getByPK(id));
		}catch(Exception ex) {	
			System.out.println(ex.getMessage());
			return new ResponseEntity(HttpStatus.NOT_FOUND);
		
		}
		
		
	}
	
	@RequestMapping(value = "/ride-requests", method = RequestMethod.POST)
	public ResponseEntity post(@RequestBody RideRequest RideRequest) {
		DAObj=new RideRequestDAO();
		try {
			DAObj.save(RideRequest);
			return new ResponseEntity(HttpStatus.OK);
		}catch(Exception ex) {	
			System.out.println(ex.getMessage());
			return new ResponseEntity(HttpStatus.NOT_FOUND);
		
		}
	}
	@RequestMapping(value = "/ride-requests/{id}", method = RequestMethod.PUT)
	public ResponseEntity update(@PathVariable("id") int id, @RequestBody RideRequest RideRequest) {
		DAObj=new RideRequestDAO();
		try {
			DAObj.update(RideRequest);
			return new ResponseEntity(HttpStatus.OK);
		}catch(Exception ex) {	
			System.out.println(ex.getMessage());
			return new ResponseEntity(HttpStatus.NOT_FOUND);
		
		}	
	}
	@DeleteMapping("/ride-requests/{id}")
	public ResponseEntity delete(@PathVariable("id") int id) {
		DAObj=new RideRequestDAO();
		try {
			DAObj.delete(id);
			return new ResponseEntity(HttpStatus.OK);
		}catch(Exception ex) {	
			System.out.println(ex.getMessage());
			return new ResponseEntity(HttpStatus.NOT_FOUND);
		
		}
	}

	
}
